function EyePosnerWrap(dummy)

commandwindow;
PsychDefaultSetup(1);


trainnumSet = 3;

testnumSet = 10;
PESTTrial = 1;
PESTCorrect = 0;
GaborTime = 0.2;
exit = 0;

try
    for session = 0:trainnumSet
        if exit == 0
            date = ['EP' datestr(now,'yyyy_mm_dd_HHMMSS')];
            edfdate = datestr(now,'HHMMSS');
            save_name = [date '_' num2str(session) '_training'];
            [exit, PESTTrial, PESTCorrect, GaborTime] = EyeTrackPosnerv2(session, ['data\' save_name],[edfdate '.edf'],PESTTrial,PESTCorrect, 0, GaborTime, dummy);
        elseif exit == 1
            break;
        end
    end
    for session = 0:testnumSet
        if exit == 0
            date = ['EP' datestr(now,'yyyy_mm_dd_HHMMSS')];
            edfdate = datestr(now,'HHMMSS');
            save_name = [date '_' num2str(session) '_test'];
            [exit, PESTTrial, PESTCorrect, GaborTime] = EyeTrackPosnerv2(session, ['data\' save_name],[edfdate '.edf'],PESTTrial,PESTCorrect, 1, GaborTime, dummy);
        elseif exit == 1
            break;
        end
    end
    ShowCursor();
    cleanup;
    windowRect
    close all;
    clear all;
catch myerr
    %this "catch" section executes in case of an error in the "try" section
    %above.  Importantly, it closes the onscreen window if its open.
    cleanup;
    commandwindow;
    myerr;
    display(myerr.message);
    myerr.stack.line
    close all;
    clear all;
end %try..catch.

% Cleanup routine:
function cleanup
% Shutdown Eyelink:
Eyelink('Shutdown');

% Close window:
sca;

% Restore keyboard output to Matlab:
ListenChar(0);