function [exit, PESTTrial, PESTCorrect, GaborTime] = EyeTrackPosnerv2(session, save_name,edf_name, PESTTrial, PESTCorrect, test, GaborTime, debug1)
fprintf('Posner Task\n\n\t');
dummymode = debug1;  %if debug1 = 0, runs normal. if debug1 = 1, forces dummy mode.
createFile = 1; %make new edfs
% STEP 1
% Open a graphics window on the main screen
% using the PsychToolbox's Screen function.

screenNumber=1;

% Define black, white and grey
white = WhiteIndex(screenNumber);
grey = white / 2;
black = BlackIndex(screenNumber);

% Open the screen
[window, windowRect] = PsychImaging('OpenWindow', screenNumber, black, [], 32, 2,...
    [], [],  kPsychNeed32BPCFloat);


% Get the centre coordinate of the window
width = windowRect(3);
height = windowRect(4);
[xCenter, yCenter] = RectCenter(windowRect);
%%
% Keyboard information
infoKey = KbName('i');
exitKey = KbName('e'); % quit the whole test
abortKey = KbName('space'); % quit session set
leftKey = KbName('1!');
rightKey = KbName('2@');
%%
% Cue information

% Number of trials
% if session == 0
%     numTrials = 5;
% else
    numTrials = 20;
% end

% Left or Right cue
cueDir = randi([0,1], 1, numTrials);
% Validity of the cue
if session == 0
    cueValidity(1:numTrials) = 1;
else
    invalidCounter = 0;
    for j = 1:numTrials
        if rand(1,1) < 0.2 && invalidCounter <2
            cueValidity(j) = 0;     % invalid trial
            invalidCounter= invalidCounter +1;
        else
            cueValidity(j) = 1;     % valid trials
            
        end
    end
end

trial = 1;

%%
% Stimulus information
% Dimension of the region where will draw the Gabor in pixels
gaborDimPix = 300;

% Sigma of Gaussian
sigma = gaborDimPix / 7;

% Obvious Parameters
contrast = 0.5;
aspectRatio = 1.0;

% Spatial Frequency (Cycles Per Pixel)
% One Cycle = Grey-Black-Grey-White-Grey i.e. One Black and One White Lobe
numCycles = 8;
freq = numCycles / gaborDimPix;

% Build a procedural gabor texture
gabortex = CreateProceduralGabor(window, gaborDimPix, gaborDimPix, [],...
    [0.5 0.5 0.5 0.0], 1, 0.5);

% Stimuli Y position
yPos(1,1:numTrials) = yCenter;
xPos = randi([0 1],1,numTrials);
cueDirCell = cell(1,numTrials);
% Stimuli X position
for k = 1:numTrials
    if cueDir(k) == 0
        cueDirCell(k) = cellstr('Left');
        xPos(k) = round(xCenter/5);
    else
        cueDirCell(k) = cellstr('Right');
        xPos(k) = round(xCenter+xCenter*4/5);
    end
end
% Count how many Gabors there are (two for session demo)


% Make the destination rectangles for  the Gabors in the array i.e.
% rectangles the size of our Gabors cenetred above an below fixation.
baseRect = [0 0 gaborDimPix gaborDimPix];
allRects = nan(4, numTrials);
for i = 1:numTrials
    allRects(:, i) = CenterRectOnPointd(baseRect, xPos(i), yPos(i));
end
% Gabor used for the information screen
sampleRect = nan(4,2);
sampleRect(:,1) = CenterRectOnPointd(baseRect, width/5, yCenter);
sampleRect(:,2) = CenterRectOnPointd(baseRect, width*4/5, yCenter);

% Randomise the phase of the Gabors and make a properties matrix.
phaseLine = rand(1, numTrials) .* 360;
propertiesMat = repmat([NaN, freq, sigma, contrast,...
    aspectRatio, 0, 0, 0], numTrials, 1);
propertiesMat(:, 1) = phaseLine';

% Set the orientations for the methods of constant stimuli. We will center
% the range around zero (vertical) and give it a range of 1.8 degress, this
% will mean we test between -(1.8 / 2) and +(1.8 / 2). Finally we will test
% seven points linearly spaced between these extremes.
baseOrientation = 0;
orRange = 10;
numSteps = 2;
stimValues = linspace(-orRange / 2, orRange / 2, numSteps) + baseOrientation;
%TODO - are these the angles of rotation?
% Now we set the number of times we want to do each condition, then make a
% full condition vector and then shuffle it. This will randomly order the
% orientation we present our Gabor with on each trial.
condVector = Shuffle(repmat(stimValues, 1, numTrials));

% Make a vector to record the response & response time for each trial
respVector = zeros(1, numTrials);
% Time Stamp data

% timeStamp(trial, id)
% id = 1 : Start of trial
% id = 2 : Cue On
% id = 3 : Cue off
% id = 4 : Stim on
% id = 5 : Stim off
% id = 6 : response
timeStamp = zeros(numTrials, 5);
%%
% Timing Information

% Presentation Time for the cue in seconds and frames
t_presCue = 0.2;

% Presentation Time for the Gabor in seconds and frames
t_presGabor = GaborTime;

%% Eyelink Setup
HideCursor(screenNumber);
% STEP 2
% Provide Eyelink with details about the graphics environment
% and perform some initializations. The information is returned
% in a structure that also contains useful defaults
% and control codes (e.g. tracker state bit and Eyelink key values).
el=EyelinkInitDefaults(window);
% Disable key output to Matlab window:
ListenChar(2);


% STEP 3
% Initialization of the connection with the Eyelink Gazetracker.
% exit program if this fails.
if ~EyelinkInit(dummymode, 1)
    fprintf('Eyelink Init aborted.\n');
    cleanup;  % cleanup function
    return;
end

[v vs]=Eyelink('GetTrackerVersion');
fprintf('Running experiment on a ''%s'' tracker.\n', vs );

% make sure that we get gaze data from the Eyelink
Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA');

% open file to record data to
edfFile=edf_name;
if createFile
    status = Eyelink('Openfile', edfFile);
    if status ~=0
        display(['openfile error, status: ' status]);
    end
end

% STEP 4
% Calibrate the eye tracker
EyelinkDoTrackerSetup(el);

% STEP 5
% start recording eye position
stats = Eyelink('StartRecording');
if status~=0
    display(['startrecording error, status: ', status]);
end
% record a few samples before we actually start displaying
WaitSecs(0.1);
% mark zero-plot time in data file
status =  Eyelink('Message', 'SYNCTIME');
if status ~=0
    display([ 'message error, status: ',status])
end
eye_used = -1;

if session == 0
    Screen('Flip', el.window);
    % Information Screen
    t2  = Screen('MakeTexture', el.window, black);
    Screen('DrawText', el.window, 'Press ANY KEY to begin the practice', 200, height-el.msgfontsize-30, el.msgfontcolour);
    
    % Draw a black cross
    Screen('DrawTexture', el.window, t2, [],[xCenter-50 yCenter-2 xCenter-30 yCenter+2], 0);
    Screen('DrawTexture', el.window, t2, [], [xCenter-50 yCenter-2 xCenter-30 yCenter+2], 90);
    Screen('DrawText', el.window, 'Left', xCenter-70, yCenter-50, el.msgfontcolour);
    
    % Draw a diagonal black cross
    Screen('DrawTexture', el.window, t2, [],[xCenter+30 yCenter-2 xCenter+50 yCenter+2], 45);
    Screen('DrawTexture', el.window, t2, [], [xCenter+30 yCenter-2 xCenter+50 yCenter+2], 135);
    Screen('DrawText', el.window, 'Right', xCenter+10, yCenter-50, el.msgfontcolour);
    
    % Set the right blend function for drawing the gabors
    Screen('BlendFunction', el.window, 'GL_ONE', 'GL_ZERO');
    
    % Draw the Gabor
    Screen('DrawTextures', el.window, gabortex, [], sampleRect(:,1), 20, [], [], [], [],...
        kPsychDontDoRotation, propertiesMat');
    Screen('DrawText', el.window, 'Left', width/5-10, yCenter-10, el.msgfontcolour);
    % Draw the Gabor
    Screen('DrawTextures', el.window, gabortex, [], sampleRect(:,2), -20, [], [], [], [],...
        kPsychDontDoRotation, propertiesMat');
    Screen('DrawText', el.window, 'Right', width*4/5-10, yCenter-10, el.msgfontcolour)
    
    Screen('DrawDots', el.window, [width-10; height-10], 10, white, [], 2);
    Screen('DrawDots', el.window, [width-10; height-10], 10, white, [], 2);
    Screen('Flip', el.window);
    KbStrokeWait;
end

% Initial Screen
correct_sound = audioread('C:\Windows\Media\tada.wav'); % load sounds to be used
incorrect_sound = audioread('C:\Windows\Media\chord.wav');
neg_sound = audioread('C:\Windows\Media\Windows Critical Stop.wav');

message='Press SPACE to stop.   Press I for instructions.    Press E to exit the test';
Screen('DrawText', el.window, message, 200, height-el.msgfontsize-30, el.msgfontcolour);
ovalCenter = [xCenter-15 yCenter-15 xCenter+15 yCenter+15];
Screen('FrameOval', el.window, black, ovalCenter,3,3);
rectCenter = [xCenter-20 yCenter-2 xCenter+20 yCenter+2];
rectWhite  = Screen('MakeTexture', el.window, white);
rectGrey  = Screen('MakeTexture', el.window, grey);
rectBlack  = Screen('MakeTexture', el.window, black);
Screen('DrawTexture', el.window, rectGrey, [], rectCenter, 0);
Screen('DrawTexture', el.window, rectGrey,[], rectCenter, 90);
Screen('DrawDots', el.window, [xCenter; yCenter], 5, black, [], 2);
Screen('Flip', el.window,1);

%% Stages: init - initialization period, cue - cue period, stim - stimulus period
% -1 - Before Calibration
% 0 - Before Ti_init
% 1 - After Tc_int, Before Ti_cue
% 2 -
% 3 - After Ti_cue, Before Tc_cue
% 4 - After Tc_cue, Before Ti_stim
% 5 - After Ti_stim presentation; the gaze need not to be at the center
% TODO: make sure gaze is still in center for stage 5, release fixation
% at stage 6
% 6 - Response captured, end of trial
stage = 0;
exit = 0;
%%  while loop
while 1
    % Check recording status, stop display if error
    status=Eyelink('CheckRecording');
    if(status~=0)
        display(['checkrecording problem, stats: ' , status]);
        break;
    end
    % check for keyboard press
    [keyIsDown, secs, keyCode] = KbCheck;
    % if spacebar was pressed stop display
    if keyCode(abortKey)
        break;
    elseif keyCode(exitKey)
        exit = 1;
        break;
    elseif keyCode(infoKey)
        stage = 0;
        % Information Screen
        Screen('Flip', el.window);
        t2  = Screen('MakeTexture', el.window, black);
        Screen('DrawText', el.window, 'Press ANY KEY to Resume', 200, height-el.msgfontsize-30, el.msgfontcolour);
        Screen('DrawTexture', el.window, t2, [],[xCenter-50 yCenter-2 xCenter-30 yCenter+2], 0);
        Screen('DrawTexture', el.window, t2, [], [xCenter-50 yCenter-2 xCenter-30 yCenter+2], 90);
        Screen('DrawText', el.window, 'Left', xCenter-70, yCenter-50, el.msgfontcolour);
        Screen('DrawTexture', el.window, t2, [],[xCenter+30 yCenter-2 xCenter+50 yCenter+2], 45);
        Screen('DrawTexture', el.window, t2, [], [xCenter+30 yCenter-2 xCenter+50 yCenter+2], 135);
        Screen('DrawText', el.window, 'Right', xCenter+10, yCenter-50, el.msgfontcolour);
        
        % Set the right blend function for drawing the gabors
        Screen('BlendFunction', el.window, 'GL_ONE', 'GL_ZERO');
        
        % Draw the Gabor
        Screen('DrawTextures', el.window, gabortex, [], sampleRect(:,1), 20, [], [], [], [],...
            kPsychDontDoRotation, propertiesMat');
        Screen('DrawText', el.window, 'Left', width/5-10, yCenter-10, el.msgfontcolour);
        % Draw the Gabor
        Screen('DrawTextures', el.window, gabortex, [], sampleRect(:,2), -20, [], [], [], [],...
            kPsychDontDoRotation, propertiesMat');
        Screen('DrawText', el.window, 'Right', width*4/5-10, yCenter-10, el.msgfontcolour)
        
        
        Screen('Flip', el.window);
        KbStrokeWait;
    end
    
    if trial > numTrials
        break;
    end
    
    % Get the Gabor angle for this trial (negative values are to the right
    % and positive to the left)
    theAngle = condVector(trial);
    thisDstRect = allRects(:, trial);
    
    % check for presence of a new sample update
    if Eyelink( 'NewFloatSampleAvailable') > 0
        % get the sample in the form of an event structure
        evt = Eyelink( 'NewestFloatSample');
        if eye_used ~= -1 % do we know which eye to use yet?
            % if we do, get current gaze position from sample
            x = evt.gx(eye_used+1); % +1 as we're accessing MATLAB array
            y = evt.gy(eye_used+1);
            % do we have valid data and is the pupil visible?
            if x~=el.MISSING_DATA && y~=el.MISSING_DATA && evt.pa(eye_used+1)>0
                Screen('DrawText', el.window, message, 200, height-el.msgfontsize-30, el.msgfontcolour);
                if stage == 7      % Record response and end the trial
                    if theAngle * response > 0 % The response was wrong
                        respVector(trial) = 0;
                        sound(incorrect_sound,44100) % makes sound notifying that the patient was incorrect or ambiguous
                    elseif theAngle*response <0 % The response was right
                        respVector(trial) = 1;
                        sound(correct_sound,44100) % makes sound notifying that the patient was correct
                        PESTCorrect = PESTCorrect + 1;
                    else % The stimulus was ambiguous
                        respVector(trial) = -1;
                        sound(incorrect_sound,44100) % makes sound notifying that the patient was incorrect or ambiguous
                    end
                    timeStamp(trial, 6) =  respSecs; % Response
                    timeStamp(trial, 7) = PESTTrial; % Trial number included in PEST
                    timeStamp(trial, 8) = t_presGabor; % Trial stimulus time
                    timeStamp(trial, 9) = PESTCorrect; % Number of correct answers in current PEST round
                    stage = 0;
                    t_presGabor = t_presGabor + PEST(PESTTrial, PESTCorrect, test); % changes presentation time for Gabor based on results.
                    if t_presGabor < 0.01
                        t_presGabor = 0.01;
                    end
                    if PESTTrial >= 10
                        PESTTrial = 1;
                        PESTCorrect = 0;
                    else
                        PESTTrial = PESTTrial + 1;
                    end
                    trial = trial +1;
                    % check if the gaze is at the center
                elseif sqrt((xCenter-x)^2 + (yCenter-y)^2 ) < 50
                    % if it does & indicate user
                    % Change the blend function to draw an antialiased fixation point
                    % in the centre of the screen
                    Screen('BlendFunction', el.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
                    Screen('FrameOval', el.window, black, ovalCenter, 3,3);
                    Screen('DrawTexture', el.window, rectGrey, [], rectCenter, 45);
                    Screen('DrawTexture', el.window, rectGrey,[], rectCenter, 135);
                    Screen('DrawDots', el.window, [xCenter; yCenter], 5, black, [], 2);
                    
                    if stage == 0
                        timeStamp(trial, 1) = Screen('Flip', el.window,[],1); % Trial start
                        stage = 1;
                    elseif stage == 1;
                        if GetSecs > timeStamp(trial,1) + 0.5 % gaze center for 0.5 sec
                            Screen('Flip', el.window,[],1)
                            stage = 2;
                        elseif GetSecs > timeStamp(trial,1) + 10 % timeout
                            stage = -1;
                        end
                    elseif stage == 2
                        % position of the cue
                        cueCenter = [xCenter-10 yCenter-2 xCenter+10 yCenter+2];
                        cueTop = [xCenter-10 yCenter-5 xCenter+10 yCenter-1];
                        cueBot = [xCenter-10 yCenter+1 xCenter+10 yCenter+5];
                        if (cueDir(trial) == 0 && cueValidity(trial) == 1) || (cueDir(trial) == 1 && cueValidity(trial) == 0)
                            % Stimulus will appear on the right
                            Screen('BlendFunction', el.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
                            %                             Screen('DrawTexture', el.window, rectBlack, [], cueCenter, 90);
                            %                             Screen('DrawTexture', el.window, rectBlack, [], cueCenter, 0);
                            Screen('DrawTexture', el.window, rectBlack, [], cueTop, -25);
                            Screen('DrawTexture', el.window, rectBlack, [], cueBot, 25);
                            Screen('DrawTexture', el.window, rectWhite, [], [windowRect(3) - 100 windowRect(4) - 85 windowRect(3) - 75 windowRect(4) - 60], 0);
                            timeStamp(trial, 2) = Screen('Flip', el.window); % Cue on
                            NetStation
                            stage = 3;
                        else
                            % Stimulus will appear on the right
                            Screen('BlendFunction', el.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
                            Screen('DrawTexture', el.window, rectBlack, [], cueTop, 25);
                            Screen('DrawTexture', el.window, rectBlack, [], cueBot, -25);
                            Screen('DrawTexture', el.window, rectWhite, [], [windowRect(3) - 100 windowRect(4) - 85 windowRect(3) - 75 windowRect(4) - 60], 0);
                            timeStamp(trial, 2) = Screen('Flip', el.window); % Cue on
                            stage = 3;
                        end
                    elseif stage == 3
                        if GetSecs-timeStamp(trial,2) > t_presCue
                            timeStamp(trial, 3) = Screen('Flip', el.window);
                            Screen('DrawTexture', el.window, rectWhite, [], [windowRect(3) - 55 windowRect(4) - 85 windowRect(3) - 30 windowRect(4) - 60], 0);
                            % time between the cue and stimulus is
                            % between 0.75 and 1.75 sec
                            timeBetween = rand(1,1)+0.75;
                            stage = 4;
                        end
                    elseif stage == 4
                        if GetSecs - timeStamp(trial,3) > timeBetween
                            % Set the right blend function for drawing the gabors
                            Screen('BlendFunction', el.window, 'GL_ONE', 'GL_ZERO');
               
                            % Draw the Gabor
                            Screen('DrawTextures', el.window, gabortex, [], thisDstRect, theAngle, [], [], [], [],...
                                kPsychDontDoRotation, propertiesMat');
                            
                            % Change the blend function to draw an antialiased fixation point
                            % in the centre of the array
                            Screen('BlendFunction', el.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
                            Screen('DrawDots', el.window, [width-10; height-10], 10, white, [], 2);
                            Screen('DrawDots', el.window, [xCenter; yCenter], 2, black, [], 2);
                            timeStamp(trial,4) = Screen('Flip', el.window);
                            stage = 5;
                        end
                    elseif stage == 5
                        if GetSecs - timeStamp(trial,4) > t_presGabor
                            timeStamp(trial, 5) =  Screen('Flip', el.window); % Stim off
                            stage = 6;
                        end
                    elseif stage == 6
                        [keyIsDown,respSecs, keyCode] = KbCheck;
                        if keyCode(leftKey)
                            response = -1;
                            Screen('DrawTexture', el.window, rectWhite, [], [windowRect(3) - 100 windowRect(4) - 85 windowRect(3) - 75 windowRect(4) - 60], 0);
                            Screen('DrawTexture', el.window, rectWhite, [], [windowRect(3) - 55 windowRect(4) - 85 windowRect(3) - 30 windowRect(4) - 60], 0);
                            stage = 7;
                        elseif keyCode(rightKey)
                            response = 1;
                            Screen('DrawTexture', el.window, rectWhite, [], [windowRect(3) - 100 windowRect(4) - 85 windowRect(3) - 75 windowRect(4) - 60], 0);
                            Screen('DrawTexture', el.window, rectWhite, [], [windowRect(3) - 55 windowRect(4) - 85 windowRect(3) - 30 windowRect(4) - 60], 0);
                            stage = 7;
                        end
                    end  
                else
                    if (stage == 3) || (stage == 4)
                        sound(neg_sound,44000) % makes bad sound notifying that focus has been lost
                    end
                    stage = 0;
                    cueDir(trial) = randi([0,1]);
                    if cueDir(trial) == 0
                        cueDirCell(trial) = cellstr('Left');
                        xPos(trial) = round(xCenter/5);
                    else
                        cueDirCell(trial) = cellstr('Right');
                        xPos(trial) = round(xCenter+xCenter*4/5);
                    end
                    Screen('FrameOval', el.window, black, ovalCenter, 3,3);
                    Screen('DrawTexture', el.window, rectGrey, [], rectCenter, 0);
                    Screen('DrawTexture', el.window, rectGrey,[], rectCenter, 90);
                    Screen('DrawDots', el.window, [xCenter; yCenter], 5, black, [], 2);
                    Screen('Flip', el.window,[],0);
                end
            end
        else % if we don't, first find eye that's being tracked
            eye_used = Eyelink('EyeAvailable'); % get eye that's tracked
            if eye_used == el.BINOCULAR; % if both eyes are tracked
                eye_used = el.LEFT_EYE; % use left eye
            end
        end
    end
end %main loop

%STEP 7 finish up: stop recording eye movements
% if session ~= 0
col_label = num2cell(1:10);
ts = num2cell(timeStamp)';
data.Trial = col_label;
data.CueDirection = cueDirCell;
data.CueValidity = num2cell(cueValidity);
data.StimXPosition = num2cell(xPos);
data.GaborAngle = num2cell(condVector);
data.StartTime = ts(1,:);
data.CueOnTime = ts(2,:);
data.CueOffTime = ts(3,:);
data.StimOnTime = ts(4,:);
data.StimOffTime = ts(5,:);
data.ResponseTime = ts(6,:);
data.PESTTrialNumber = ts(7,:);
data.StimTime = ts(8,:);
data.PESTCorrect = ts(9,:);
data.Correctness = num2cell(respVector);
%     data = struct('Trial', col_label,'CueDirection',cueDirCell,'CueValidity', num2cell(cueValidity),...
%         'StimXPosition', num2cell(xPos),'GaborAngle', num2cell(condVector),...
%         'StartTime',ts(1,:),'CueOnTime', ts(2,:), 'CueOffTime', ts(3,:),'StimOnTime',ts(4,:), 'StimOffTime', ts(5,:),...
%         'ResponseTime', ts(6,:), 'Correctness', num2cell(respVector));
save(save_name,'data');
% end

Eyelink('StopRecording');
Eyelink('CloseFile');
% download data file
try
    fprintf('Receiving data file ''%s''\n', edfFile );
    status=Eyelink('ReceiveFile');
    if status > 0
        fprintf('ReceiveFile status %d\n', status);
    end
    if 2==exist(edfFile, 'file')
        fprintf('Data file ''%s'' can be found in ''%s''\n', edfFile, pwd );
    end
catch rdf
    fprintf('Problem receiving data file ''%s''\n', edfFile );
    display(rdf);
end

cleanup;




function cleanup
% Shutdown Eyelink:
% Eyelink('Shutdown');

% Close window:
sca;

% Restore keyboard output to Matlab:
% ListenChar(0);